//
//  ViewController.m
//  BlocksEgOC
//
//  Created by Dayanithi Natarajan on 2/24/16.
//  Copyright © 2016 Dayanithi Natarajan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


 - (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self sampleTestFunction];
    
    NSString *(^greetings)(void) = ^(void){
        return @"Good Evening";
    };
    
    
    int (^myFirstBlock)(NSString *,NSString *);
    
    
    myFirstBlock = ^(NSString *name1,NSString *name2){
        
        return 10;
    };
    
    NSLog(@"%d",myFirstBlock(@"Hi",@"Hello"));
    NSLog(@"*************");
    NSLog(@"%@",greetings);
    NSLog(@"%@", [NSDate date]);
    NSDate *(^todayDate)(void) = ^(void){
        return [NSDate date];
    };
    NSLog(@"Today's date --> %@",todayDate());
    
    
    int factor = 44;
    int (^newResult)(void) = ^(void){
        return factor*2;
    };
    NSLog(@"%d,%d",newResult(),factor);
    
    [self addNumber:5 withNumber:8 andCompletionHandler:^(int result) {
        NSLog(@"Block is not called yet");
        NSLog(@"%d",result);
        NSLog(@"Block is completed");
    }];
}

- (void)sampleTestFunction {
    __block int someValue = 10;
    int (^myOperation)(void) = ^(void){
        someValue += 5;
        return someValue+10;
    };
    NSLog(@"%d",myOperation());
}

- (void)addNumber:(int)number1 withNumber:(int)number2 andCompletionHandler:(void (^)(int))completionHandler {
    int result = number1 + number2;
    completionHandler(result);
    NSLog(@"addNumber defintion");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dddddd {
 
    int (^block_ex)(int,int);
    
    __block int newNumer = 10;
    
    block_ex = ^(int a, int b){
        newNumer = newNumer + 33;
        return a +b;
    };
    
    NSLog(@"Block_ex %d",block_ex(10,20));

}

@end
