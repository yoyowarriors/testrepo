//
//  ViewController.h
//  BlocksEgOC
//
//  Created by Dayanithi Natarajan on 2/24/16.
//  Copyright © 2016 Dayanithi Natarajan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (void)sampleTestFunction;
- (void)addNumber:(int)number1 withNumber:(int)number2 andCompletionHandler:(void(^)(int result))completionHandler;
@end

